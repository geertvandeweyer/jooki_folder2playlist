#!/usr/bin/env python

import json
import sys
import glob
import hashlib
import subprocess
import argparse
import os
from shutil import copyfile
from time import time

def md5_16(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()[:16]
	

def get_meta(file):
	ext = file.rsplit(".",1)[1]
	meta = {'artist':'unknown','album':'unknown','title':'unknown','tracknumber':1} 
	meta['ext'] = ext
	meta['size'] = os.path.getsize(file)

	#if ext == 'flac':
	#	cmd = ["metaflac", "--show-tag=artist", "--show-tag=album", "--show-tag=title", "--show-tag=tracknumber", file]
	#	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
	#	for line in proc.stdout.readlines():
	#		l = line.rstrip().decode().split("=",1)
	#		meta[l[0]] = l[1]
	#	# length
	#	cmd = ['metaflac','--show-total-samples', '--show-sample-rate',file]
	#	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
	#	s_sr = []
	#	for line in proc.stdout.readlines():
	#		s_sr.append(float(line.rstrip().decode())
	#	
	#	meta['duration'] = s_sr[0]/s_sr[1]

	#elif ext == 'mp3':
	cmd = ["ffprobe", "-loglevel", "error", "-show_entries", "format_tags=artist,album,title,track","-show_entries","format=duration", "-of", "default=noprint_wrappers=1:nokey=0",file]
	proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
	for line in proc.stdout.readlines():
		l = line.rstrip().decode().split("=",1)
		l[0] = l[0].lower().replace("tag:","")
		if l[0] == "track":
			l[0] = 'tracknumber'
		meta[l[0]] = l[1]
	
	return(meta)

def arg_parser():
	parser = argparse.ArgumentParser(description='Convert music folders to jooki playlists')
	parser.add_argument('-j','--jooki_dir',dest='jooki_dir',help='location where the jooki SD card is mounted',required=True)
	parser.add_argument('-d','--music_dir',dest='music_dirs',action='append',help='folder(s) to add to the playlist. Can be specified multiple times',required=True)
	parser.add_argument('-n','--playlist_name',dest='playlist_name',help='playlist name (default: artist-album)',default=False)
	parser.add_argument('-r','--recursive',dest='recursive',action='store_true',help='Add music files recursively from input dirs (default: false)')
	args = parser.parse_args()

	## validate arguments.
	if not os.path.isdir(args.jooki_dir):
		print("\n\nERROR: provided jooki mount point is not a valid folder: "+args.jooki_dir+"\n\n")
		parser.print_help()
		sys.exit(2)	

	for dir in args.music_dirs:
		if not os.path.isdir(dir):
			print("\n\nERROR: provided music directory is  not a valid folder: "+dir+"\n\n")
			parser.print_help()
			sys.exit(2)

	# required files present?
	if not 	os.path.isfile(args.jooki_dir+"/tracks.json") or not os.path.isfile(args.jooki_dir+"/playlists.json"):
		print("\n\nERROR: provided jooki mount point does not contain tracks.json and playlists.json\n\n")
		sys.exit(2)	
		
	return(args)	



def main():
	# get arguments
	args = arg_parser()

	# load tracks json.	
	with open(args.jooki_dir+'/tracks.json') as f:
		tracks = json.load(f)

	# create backup files.
	try:
		copyfile(args.jooki_dir+"/playlists.json",args.jooki_dir+"/playlists.json.bak")
	except:
		print("\n\n ERROR: unable to create backup playlist file. Check permissions!\n")
		sys.exit(2)

	try: 
		copyfile(args.jooki_dir+"/tracks.json",args.jooki_dir+"/tracks.json.bak")
	except: 
		print("\n\n ERROR: unable to create backup tracks file. Check permissions!\n")
		sys.exit(2)

	## tracks in the playlist:
	pl_tracks = []
	songs = []
	# process music files.
	for music_dir in args.music_dirs:
		files = []
		for type in ['flac','mp3']:
			if args.recursive:
				files.extend(glob.glob(music_dir+"/**/*."+type,recursive=True))
			else:
				files.extend(glob.glob(music_dir+"/*."+type))
		for file in files:
			md5_hash = md5_16(file)
			meta = get_meta(file)
			if not args.playlist_name:
				args.playlist_name = meta['artist']+"_"+meta['album']
				print("Playlist name not provided. Set to '"+args.playlist_name+"'")
			
			# add to overall track dict
			tracks[md5_hash] = {"album":meta['album'],"artist":meta['artist'],"codec":meta['ext'],"duration":str(meta["duration"]),"filename":"/jooki/external/jooki/uploads/"+md5_hash,"format":meta['ext'],"hasImage":False,"size":str(meta['size']),"title":meta['title']}
			# copy to jooki folder.
			try: 
				copyfile(file,args.jooki_dir+"/uploads/"+md5_hash)
			except: 
				print("\n\n MESSAGE: unable to copy file to jooki upload folder:\n")
				print("   => skipped file: "+file+"\n")
				continue 
			# add to playlist
			pl_tracks.append(md5_hash)
			songs.append(meta['artist']+" : "+meta['title'])

	if len(pl_tracks) == 0:
		print("\n\n MESSAGE: No music files found, not creating a playlist!\n\n")	
		sys.exit(0)

	# write tracks file .	
	with open(args.jooki_dir+"/tracks.json.new","w") as outfile:	
		json.dump(tracks,outfile,indent=4)
	
	# write playlists file 
	with open(args.jooki_dir+"/playlists.json.new",'w') as f:
		# all but last two lines are kept.
		subprocess.call(["head","-n","-2",args.jooki_dir+"/playlists.json"],stdout=f)
		# reclose last playlist
		f.write("  },"+"\n")
		# dump the generated playlist
		f.write('  "user_'+str(round(time()))+'": {'+"\n")
		f.write('    "audiobook": false,'+"\n")
		f.write('    "title": "'+args.playlist_name+'",'+"\n")
		f.write('    "tracks": [ "'+'", "'.join(map(str,pl_tracks))+'" ]'+"\n")
		f.write('  }'+"\n")
		f.write('}'+"\n")

	# finally : if here, all ok, copy new to final.
	try: 
		copyfile(args.jooki_dir+"/playlists.json.new",args.jooki_dir+"/playlists.json")
	except: 
		print("\n\n ERROR: unable to write new playlist file:\n");
		print("   - Advised to manually restore backup file: \n");
		print("      > mv '"+args.jooki_dir+"/playlists.json.bak' '"+args.jooki_dir+"/playlists.json'\n\n")
		sys.exit(2)
		
	try:
		copyfile(args.jooki_dir+"/tracks.json.new",args.jooki_dir+"/tracks.json")
	except: 
		print("\n\n ERROR: unable to write new tracks file:\n");
		print("   - Advised to manually restore backup files: \n");
		print("      > mv '"+args.jooki_dir+"/playlists.json.bak' '"+args.jooki_dir+"/playlists.json'\n")
		print("      > mv '"+args.jooki_dir+"/tracks.json.bak' '"+args.jooki_dir+"/tracks.json'\n\n")
		sys.exit(2)


	## exit.
	print("\n\nCreated playlist '"+args.playlist_name+"', containing the following songs:\n - "+"\n - ".join(songs)+"\n\n")

if __name__ == "__main__":
    main()
